import os

import Tower
import Disc

tower1 = Tower.Tower(1)
tower2 = Tower.Tower(2)
tower3 = Tower.Tower(3)

for i in range(3, 0, -1):
    tempDisc = Disc.Disc(i)
    tower1.addDiscToTower(tempDisc)

towers = []
towers.append(tower1)
towers.append(tower2)
towers.append(tower3)


def gameLoop():
    #fetch user input for next move
    moveOrigin = input("Specify the tower from which you want to move a disc. Enter the number of the tower: ")
    moveDestination = input("Specify the tower to which you want to move a disc. Enter the number of the tower: ")

    #perform move
    towerOrigin = towers[moveOrigin - 1]
    towerDestination = towers[moveDestination - 1]

    print(str(towerOrigin.towerId) + " " + str(towerDestination.towerId))

    discToMove = towerOrigin.getDiscOnTop()
    if discToMove == 0:
        print("Tower does not contain discs!")
        return

    if towerDestination.addDiscToTower(discToMove) == 0:
        print("Disc is too huge!")
        return

    #remove disc from other tower
    towerOrigin.removeTopDisc()

def isGameFinished():
    gameIsFinished = 0
    if tower3.getNumberOfPiecesOnTower() == 3:
        gameIsFinished = 1
        print("Congratulations you finished the game!")

    return gameIsFinished


def printTowers():
    listOfAllTowerContents = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for tower in towers:
        currentDiscList = tower.discs
        currentDiscList.reverse()
        currentTowerId = tower.getTowerId() - 1
        if len(currentDiscList) > 0:
            for i in range(0, len(currentDiscList)):
                listOfAllTowerContents[currentTowerId + (i * 3)] = (currentDiscList[i]).size
        else:
            listOfAllTowerContents.append(0)

    count = 0
    towerAsString = ""
    for entry in listOfAllTowerContents:
        if entry == 0:
            towerAsString += "     " + "\t"
        else:
            towerAsString += "   " + str(entry) + "\t"
        if count == 2:
            towerAsString += ("\n")
            count = -1

        count = count + 1

    towerAsString += "\nTower 1" + "\t" + "Tower 2" + "\t" + "Tower 3" + "\n"
    print towerAsString
    towerAsString = ""

#continue until game is completed
while isGameFinished() == 0:
    os.system('cls' if os.name == 'nt' else 'clear')
    printTowers()
    gameLoop()
