import Disc

class Tower:

    def __init__(self, towerId):
        self.towerId = towerId
        self.discs = []

    def addDiscToTower(self, disc):
        result = 1

        #compare new entry against last element
        if len(self.discs) > 0:
            if disc > self.discs[-1]:
                self.discs.append(disc)
            else:
                result = 0
        else:
            self.discs.append(disc)

        return result

    def removeTopDisc(self):
        self.discs.pop()

    def getTowerId(self):
        return self.towerId

    def getNumberOfPiecesOnTower(self):
        return len(self.discs)

    def getDiscOnTop(self):
        topDisc = 0
        if len(self.discs) > 0:
            topDisc = self.discs[-1]
        return topDisc